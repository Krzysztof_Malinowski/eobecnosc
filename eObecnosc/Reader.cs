﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCSC;
using PCSC.Iso7816;
using System.Diagnostics;
using System.Windows;

namespace eObecnosc
{
    class Reader
    {
        public List<string> GetNameReaders()
        {
            List<string> names = new List<string>();

            using (var context = new SCardContext())
            {
                context.Establish(SCardScope.System);

                var readerNames = context.GetReaders();
                foreach (var item in readerNames)
                {
                    Trace.WriteLine(item);
                    names.Add(item);
                }
            }

            return names;
        }

        public string GetUID(string aChoiceReader)
        {

            using (var context = new SCardContext())
            {
                context.Establish(SCardScope.System);

                String readerName = aChoiceReader;

                if (readerName == null)
                {
                    return "x";
                }


                using (var isoReader = new IsoReader(context, readerName, SCardShareMode.Shared, SCardProtocol.Any, false))
                {

                    var apdu = new CommandApdu(IsoCase.Case2Short, isoReader.ActiveProtocol)
                    {
                        CLA = 0xFF, // Class
                        Instruction = InstructionCode.GetData,
                        P1 = 0x00,  // Parameter 1
                        P2 = 0x00,  // Parameter 2
                        Le = 0x00   // Expected length of the returned data
                    };

                    Trace.WriteLine("Send APDU with command: {0}", BitConverter.ToString(apdu.ToArray()));
                    var response = isoReader.Transmit(apdu);

                    Trace.WriteLine("SW1 SW2 = " + String.Format("{0:X}", response.SW1) + " " + response.SW2);

                    if (!response.HasData)
                    {
                        Trace.WriteLine("No data. (Card does not understand \"GET CHALLENGE\")");
                        return "x";
                    }
                    else
                    {
                        var data = response.GetData();
                        Trace.WriteLine("Response: " + BitConverter.ToString(data));
                        return BitConverter.ToString(data);
                    }
                }
            }
        }
    }
}
