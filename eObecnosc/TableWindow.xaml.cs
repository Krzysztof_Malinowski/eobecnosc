﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Data;

namespace eObecnosc
{
    /// <summary>
    /// Interaction logic for TableWindow.xaml
    /// </summary>
    public partial class TableWindow : Window
    {
        User user;
        public TableWindow(string aUID)
        {
            InitializeComponent();
            user = new User(aUID);
            Trace.WriteLine(user.UID);
           // table.Items.Add("xxx");
        }
        void FillingDataGrid()
        {
            DataTable dt = new DataTable();
            DataColumn id = new DataColumn("ID",typeof(int));
            DataColumn uid = new DataColumn("UID",typeof(string));
            DataColumn name = new DataColumn("name",typeof(string));
 
            dt.Columns.Add(id);
            dt.Columns.Add(uid);
            dt.Columns.Add(name);

            for(int i = 1; i < 20; i++)
            {
                DataRow row = dt.NewRow();
                row[0] = i;
                row[1] = user.UID;
                row[2] = "Jan kowalski";
                dt.Rows.Add(row);
            }
            
            table.ItemsSource = dt.DefaultView;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Loaded ...");
            FillingDataGrid();
        }

        private void Registration_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
