﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eObecnosc
{
    class User
    {
        private string uid;

        public User(string uid)
        {
            this.uid = uid;
        }

        public string UID { get => uid; set => uid = value; }

        public bool SendUID()
        {
            return true;
        }
    }
}
