﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PCSC;
using PCSC.Iso7816;
using System.Diagnostics;

namespace eObecnosc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String choiceReader = null;
        Reader Reader;
        User user;

        public MainWindow()
        {
            Trace.WriteLine("Start ...");
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Loaded ...");

            Reader = new Reader();

            foreach (var item in Reader.GetNameReaders())
            {
                reader.Items.Add(item);
            }
            
            foreach (var item in APIRepository.GetRooms())
            {
                room.Items.Add(item);
            }

            foreach (var item in APIRepository.GetSubjects())
            {
                subject.Items.Add(item);
            }
            //APIRepository.testAsync();
        }

        private void Registration_Click(object sender, RoutedEventArgs e)
        {
            if (choiceReader != null)
            {
                string uid = "x";
                try
                {
                     uid = Reader.GetUID(choiceReader);
                }   
                catch (PCSC.Exceptions.RemovedCardException)
                {
                    MessageBox.Show("Brak karty", " ", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch
                {
                    MessageBox.Show("Błąd odczytu karty", " ", MessageBoxButton.OK, MessageBoxImage.Error);
                }
   
                if(uid != "x")
                {
                    user = new User(uid);

                    if (user.SendUID())
                    {
                        Trace.WriteLine("NOWY WIDOK");
                        TableWindow win = new TableWindow(user.UID);
                        win.Show();

                        this.Close();
                    }
                }
               
            }
            else
            {
                MessageBox.Show("Wybierz czujnik", " ", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Room_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Subject_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Reader_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Trace.WriteLine(reader.SelectedItem.ToString());
            choiceReader = reader.SelectedItem.ToString();
        }
    }
}
