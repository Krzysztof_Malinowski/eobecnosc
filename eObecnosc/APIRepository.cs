﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace eObecnosc
{

    public class ClassRoom
    {
 
        public int id { get; set; }
        public string name { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    static class APIRepository
    {
        private static readonly HttpClient client = new HttpClient();
        static public List<String> GetSubjects()
        {
            List<String> response = new List<string>
            {
                "AUTOMATYKA",
                "ROBOTYKA",
                "PKM"
            };
            return response;
        }

        static public List<String> GetRooms()
        {
            List<String> response = new List<string>
            {
                "sala 124",
                "sala 125",
                "sala 126"
            };
            return response;
        }
        static public async Task testAsync()
        {
            client.DefaultRequestHeaders.Add("ekonto_id","1234");
            var values = new Dictionary<string, string>
            {
               { "ekonto_id", "1234" }
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://127.0.0.1:8000/api/getClassrooms", content);
           
            var responseString = await response.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<object>(responseString);
        }
    }
}
